package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    void bindTaskToProject(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void unbindTaskToProject(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId);

    void removeProjectById(@NotNull final String userId, @Nullable final String projectId);

}
