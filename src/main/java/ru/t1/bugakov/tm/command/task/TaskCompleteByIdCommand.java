package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

}
