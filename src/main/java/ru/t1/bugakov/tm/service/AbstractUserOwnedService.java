package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.api.service.IAbstractUserOwnedService;
import ru.t1.bugakov.tm.exception.entity.ModelNotFoundException;
import ru.t1.bugakov.tm.exception.field.IdEmptyException;
import ru.t1.bugakov.tm.exception.field.IndexIncorrectException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>> extends AbstractService<M, R> implements IAbstractUserOwnedService<M> {

    protected AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(userId, id);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new UserIdEmptyException();
        findById(userId, id);
        return true;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) throw new UserIdEmptyException();
        repository.remove(userId, model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

}
