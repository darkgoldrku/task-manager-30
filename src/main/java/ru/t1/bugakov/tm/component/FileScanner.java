package ru.t1.bugakov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileScanner extends Thread {


    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    public final File folder = new File("./");

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    public void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!this.isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    private void process() {
        for (@NotNull File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
